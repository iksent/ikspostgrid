<?php

add_action('admin_menu', 'iks_menu_init');
function iks_menu_init() {
    //create new top-level menu
    add_menu_page('Iks Post Grid', 'Iks Post Grid', 'administrator', __FILE__, 'iks_settings_page', plugins_url('images/icon.png', __FILE__));
    //call register settings function
    add_action('admin_init', 'iks_init_settings_arrays');
    add_action('admin_init', 'iks_register_settings');
}

/**
 * Подключаем Iris Color Picker
 */
add_action('admin_enqueue_scripts', 'iks_add_admin_iris_scripts');
function iks_add_admin_iris_scripts($hook) {
    // подключаем IRIS
    wp_enqueue_script('wp-color-picker');
    wp_enqueue_style('wp-color-picker');
    // подключаем свой файл скрипта
    wp_enqueue_script('plugin-scripts', plugins_url('js/plugin-scripts.js', __FILE__), array('wp-color-picker'), false, 1);
}


$default_settings = array();
/**
 * All Settings
 * OptionName => DefaultValue
 */
function iks_init_settings_arrays() {
    global $default_settings;
    $default_settings = array(
        'iks-settings-group' => array(
            /* screen widths */
            'iks_is_adaptive_enabled' => '1',
            'iks_computer_screen_width' => '1100',
            'iks_mobile_screen_width' => '768',
            /* computer columns */
            'iks_computer_columns_count' => '3',
            'iks_computer_between_columns_width' => '1',
            'iks_computer_between_rows_width' => '15',
            /* tablet columns */
            'iks_tablet_columns_count' => '2',
            'iks_tablet_between_columns_width' => '1',
            'iks_tablet_between_rows_width' => '15',
            /* mobile columns */
            'iks_mobile_columns_count' => '1',
            'iks_mobile_between_columns_width' => '1',
            'iks_mobile_between_rows_width' => '7',
            /* post */
            'iks_post_padding' => '',
            'iks_post_box_shadow' => '',
            'iks_post_border' => '',
            'iks_post_border_radius' => '',
            'iks_post_text_align' => 'left',
            /* images */
            'iks_image_height' => '190',
            'iks_image_margin' => '',
            'iks_image_padding' => '',
            'iks_image_box_shadow' => '',
            'iks_image_border' => '',
            'iks_image_border_radius' => '',
            'iks_image_stretching' => 'cover',
            'iks_image_quality' => 'medium',
            'iks_no_image_action' => 'standard',
            /* link button */
            'iks_button_width' => '100',
            'iks_button_align' => 'center',
            'iks_button_text_align' => 'center',
            'iks_button_margin' => '',
            'iks_button_padding' => '10px',
            'iks_button_box_shadow' => '',
            'iks_button_border' => '',
            'iks_button_border_radius' => '5px',
            'iks_button_background_color' => '#2191b6',
            'iks_button_text_color' => '#ffffff',
            /* link button on hover */
            'iks_button_box_shadow_hover' => '',
            'iks_button_border_hover' => '',
            'iks_button_border_radius_hover' => '',
            'iks_button_background_color_hover' => '#1080a5',
            'iks_button_text_color_hover' => '#eee',
            /* content settings */
            'iks_is_enabled_content_settings' => '',
            'iks_is_enabled_image' => '',
            'iks_is_enabled_title' => '',
            'iks_is_enabled_comments_link' => '',
            'iks_is_enabled_date' => '',
            'iks_is_enabled_categories' => '',
            'iks_is_enabled_content_preview' => '',
            'iks_is_enabled_button_link' => '',
            'iks_button_link_text' => '',
            'iks_is_enabled_pagination' => '',
            /* custom css */
            'iks_post_custom_css' => '',
            'iks_custom_css' => '',
        )
    );
}

/**
 * register plugin's settings
 */
function iks_register_settings() {
    global $default_settings;
    foreach ($default_settings as $group => $settings) {
        foreach ($settings as $name => $value) {
            register_setting($group, $name);
        }
    }
}

/**
 * register plugin's settings
 */
function iks_reset_settings() {
    global $default_settings;
    foreach ($default_settings as $group => $settings) {
        foreach ($settings as $name => $value) {
            update_option($name, $value);
        }
    }
}

/**
 * Function executes when plugin activates
 * register_activation_hook(__FILE__, 'iks_init_default_settings');
 */
function iks_init_default_settings() {
    global $default_settings;
    foreach ($default_settings as $group => $settings) {
        foreach ($settings as $name => $value) {
            if ($value != '') {
                if (get_option($name) == '') {
                    update_option($name, $value);
                }
            }
        }
    }
}

function get_settings_header($name, $description, $is_full_width) {
    $output = '';

    if ($is_full_width) {
        $output .= '<div class="settings-row">'; // #1
    } else {
        $output .= '<div class="settings-block">'; // #1
    }
    $output .= '<h4 class="settings-header">' . $name . '</h4>';
    $output .= '<div class="settings-content">'; // #2
    if ($description != '') {
        $output .= '
                    <div class="settings-description">
                        <div class="desc-icon dashicons-before dashicons-admin-post"></div>
                        ' . $description . '
                    </div>
                ';
    }
    $output .= '<table class="form-table">'; // #3
    return $output;
}

function get_settings_footer() {
    $output = '';

    $output .= '</table>'; // #3
    $output .= '</div>'; // #2
    $output .= '</div>'; // #1
    return $output;
}

function get_settings_row($name, $subtext, $examples, $setting_html) {
    $output = '';

    $output .= '<tr valign="top">';
    // TH
    $output .= '<th scope="row"><div class="main-text">' . $name . '</div>';
    if ($subtext != '') {
        $output .= '<div class="subtext">' . $subtext . '</div>';
    }
    if ($examples != '') {
        foreach ($examples as $example) {
            $output .= '<div class="example">' . $example . '</div>';
        }
    }
    $output .= '</th>';
    // TD
    $output .= '<td>' . $setting_html . '</td>';
    $output .= '</tr>';
    return $output;
}

function get_settings_rows($settings) {
    $output = '';

    foreach ($settings as $key => $setting) {
        $output .= get_settings_row($setting['name'], $setting['description'], $setting['examples'], $setting['value']);
    }
    return $output;
}

function print_settings_block($settings_name, $settings_description, $settings, $is_full_width = false) {
    echo get_settings_header($settings_name, $settings_description, $is_full_width);
    echo get_settings_rows($settings);
    echo get_settings_footer();
}

function print_reset_button() {
    $reset_url = $_GET;
    $reset_url['reset_settings'] = '1';
    $reset_url = http_build_query($reset_url);
    echo '<a href="' . $_SERVER['PHP_SELF'] . '?' . $reset_url . '"
    onclick="return confirm(\'Reset ALL plugin settings to default values?\')"
    class="button-primary">' . __('Reset Settings') . '</a>';
}

function print_save_button() {
    echo '
        <p class="submit" style="margin-top: 0;">
            <input type="submit" class="button-primary" value="' . __('Save Changes') . '"/>
        </p>
    ';
}

function print_fixed_save_button() {
    echo '
        <p class="submit fixed-submit">
            <input type="submit" class="button-primary" value="' . __('Save') . '"/>
        </p>
    ';
}

function process_reset_settings($current_url) {
    if ($current_url['reset_settings'] == '1') {
        // TODO:
//        echo '<div class="notice notice-success is-dismissible"><p>' . __('Settings 123 123 312updated') . '</p></div>';
        iks_reset_settings();
        unset($current_url['reset_settings']);
        $current_url = http_build_query($current_url);
        wp_redirect($_SERVER["PHP_SELF"] . '?' . $current_url);
        exit;
    }
}

/**
 * Text type
 */
function get_type_text($setting_name) {
    return '<input type="text" name="' . $setting_name . '"
        value="' . get_option($setting_name) . '"/>';
}

/**
 * TextArea type
 */
function get_type_textarea($setting_name) {
    return '<textarea type="text" name="' . $setting_name . '" cols="90" rows="10">
        ' . get_option($setting_name) . '</textarea>';
}

/**
 * Select type
 */
function get_type_select($setting_name, $options) {
    $output = '';

    $output .= '<select name="' . $setting_name . '">';
    foreach ($options as $option_value => $option_name) {
        $selected = '';
        if (get_option($setting_name) == $option_value) {
            $selected = 'selected';
        }
        $output .= '<option value="' . $option_value . '" ' . $selected . '>' . $option_name . '</option>';
    }
    $output .= '</select>';
    return $output;
}
function get_type_select_horizontal_align($setting_name) {
    return get_type_select($setting_name, array('left' => __('Left'), 'center' => __('Center'), 'right' => __('Right')));
}

/**
 * Color type
 */
function get_type_color($setting_name) {
    return '<input type="text" class="iks-color" name="' . $setting_name . '"
        value="' . get_option($setting_name) . '"/>';
}

/**
 * Checkbox type
 */
function get_type_checkbox($setting_name) {
    return '<input type="checkbox" name="' . $setting_name . '"
        value="1" ' . ((get_option($setting_name)) ? ("checked") : "") . ' />';
}

/**
 * Number type
 */
function get_type_number($setting_name, $min = '', $max = '') {
    return '<input type="number" name="' . $setting_name . '"
        min="' . $min . '" max="' . $max . '"
        value="' . get_option($setting_name) . '"/>';
}

/**
 * plugin's settings
 */
function iks_settings_page() {
    $current_url = $_GET;

    process_reset_settings($current_url);

    if (isset($current_url['settings-updated'])) :
        echo '<div class="notice notice-success is-dismissible"><p>' . __('Settings updated') . '</p></div>';
    endif;

    echo '
    <div class="wrap iks-setting">
        <div><h2 class="header">' . __('Iks Post Grid Settings') . '</h2></div>
        <form method="post" action="options.php">
        ';

    // wordpress settings field for update form values
    settings_fields('iks-settings-group');

    print_fixed_save_button();

    print_save_button();

    /* Screen Widths */
    $settings = array(
        'iks_is_adaptive_enabled' => array(
            'name' => __('Enable adaptive settings?'),
            'description' => __('Enable or Disable adaptive column settings for Tablets and Mobiles'),
            'value' => get_type_checkbox('iks_is_adaptive_enabled')
        ),
        'iks_computer_screen_width' => array(
            'name' => __('Computer screen width'),
            'description' => '(px) (> 1100px)',
            'value' => get_type_number('iks_computer_screen_width', '1100', '2000')
        ),
        'iks_mobile_screen_width' => array(
            'name' => __('Mobile screen width'),
            'description' => '(px) (< 1099px)',
            'value' => get_type_number('iks_mobile_screen_width', '0', '1099')
        ),
    );
    print_settings_block(__('Screen Widths'), '', $settings);

    /* Computers */
    $settings = array(
        'iks_computer_columns_count' => array(
            'name' => __('Columns count in Row'),
            'description' => sprintf(__('from %d to %d'), 1, 10),
            'value' => get_type_number('iks_computer_columns_count', '1', '10')
        ),
        'iks_computer_between_columns_width' => array(
            'name' => __('Between columns width'),
            'description' => sprintf(__('in percents - from %d to %d'), 0, 100),
            'value' => get_type_number('iks_computer_between_columns_width', '0', '100')
        ),
        'iks_computer_between_rows_width' => array(
            'name' => __('Between rows width'),
            'description' => __('in pixels'),
            'value' => get_type_number('iks_computer_between_rows_width', '0', '300')
        ),
    );
    print_settings_block(__('Columns (Computer)'), __('Works for tablets and mobiles too, if adaptive settings are NOT enabled'), $settings);

    /* Tablets */
    $settings = array(
        'iks_tablet_columns_count' => array(
            'name' => __('Columns count in Row'),
            'description' => sprintf(__('from %d to %d'), 1, 10),
            'value' => get_type_number('iks_tablet_columns_count', '1', '10')
        ),
        'iks_tablet_between_columns_width' => array(
            'name' => __('Between columns width'),
            'description' => sprintf(__('in percents - from %d to %d'), 0, 100),
            'value' => get_type_number('iks_tablet_between_columns_width', '0', '100')
        ),
        'iks_tablet_between_rows_width' => array(
            'name' => __('Between rows width'),
            'description' => __('in pixels'),
            'value' => get_type_number('iks_tablet_between_rows_width', '0', '300')
        ),
    );
    print_settings_block(__('Columns (Tablet)'), __('Works only, if adaptive settings are enabled'), $settings);

    /* Mobile */
    $settings = array(
        'iks_mobile_columns_count' => array(
            'name' => __('Columns count in Row'),
            'description' => sprintf(__('from %d to %d'), 1, 10),
            'value' => get_type_number('iks_mobile_columns_count', '1', '10')
        ),
        'iks_mobile_between_columns_width' => array(
            'name' => __('Between columns width'),
            'description' => sprintf(__('in percents - from %d to %d'), 0, 100),
            'value' => get_type_number('iks_mobile_between_columns_width', '0', '100')
        ),
        'iks_mobile_between_rows_width' => array(
            'name' => __('Between rows width'),
            'description' => __('in pixels'),
            'value' => get_type_number('iks_mobile_between_rows_width', '0', '300')
        ),
    );
    print_settings_block(__('Columns (Mobile)'), __('Works only, if adaptive settings are enabled'), $settings);

    /* Appearance of each post */
    $settings = array(
        'iks_post_padding' => array(
            'name' => __('Padding'),
            'description' => __('top right bottom left'),
            'examples' => array('3px 3px 5px 5px', '10px'),
            'value' => get_type_text('iks_post_padding')
        ),
        'iks_post_box_shadow' => array(
            'name' => __('Box-Shadow'),
            'description' => __('x y blur spread color |inset|initial|inherit'),
            'examples' => array('0 0 5px #000', '2px 2px 0 0 #aaabbb'),
            'value' => get_type_text('iks_post_box_shadow')
        ),
        'iks_post_border' => array(
            'name' => __('Border'),
            'description' => __('width || style || color'),
            'examples' => array('1px solid #000', '10px dashed #eaeaea'),
            'value' => get_type_text('iks_post_border')
        ),
        'iks_post_border_radius' => array(
            'name' => __('Border-radius'),
            'description' => __('top-left top-right bottom-right bottom-left'),
            'examples' => array('3px 3px 5px 5px', '10px'),
            'value' => get_type_text('iks_post_border_radius')
        ),
        'iks_post_text_align' => array(
            'name' => __('Text-align'),
            'description' => __('left, center, right'),
            'value' => get_type_select_horizontal_align('iks_post_text_align'),
        ),
    );
    print_settings_block(__('Appearance of each post'), '', $settings);

    /* Appearance of images */
    $settings = array(
        'iks_image_height' => array(
            'name' => __('Image height'),
            'description' => sprintf(__('Image height in pixels from %d to %d'), 1, 1000),
            'value' => get_type_number('iks_image_height', '1', '1000')
        ),
        'iks_image_margin' => array(
            'name' => __('Margin'),
            'description' => __('top right bottom left'),
            'examples' => array('3px 3px 5px 5px', '10px'),
            'value' => get_type_text('iks_image_margin')
        ),
        'iks_image_padding' => array(
            'name' => __('Padding'),
            'description' => __('top right bottom left'),
            'examples' => array('3px 3px 5px 5px', '10px'),
            'value' => get_type_text('iks_image_padding')
        ),
        'iks_image_box_shadow' => array(
            'name' => __('Box-Shadow'),
            'description' => __('x y blur spread color |inset|initial|inherit'),
            'examples' => array('0 0 5px #000', '2px 2px 0 0 #aaabbb'),
            'value' => get_type_text('iks_image_box_shadow')
        ),
        'iks_image_border' => array(
            'name' => __('Border'),
            'description' => __('width || style || color'),
            'examples' => array('1px solid #000', '10px dashed #eaeaea'),
            'value' => get_type_text('iks_image_border')
        ),
        'iks_image_border_radius' => array(
            'name' => __('Border-radius'),
            'description' => __('top-left top-right bottom-right bottom-left'),
            'examples' => array('3px 3px 5px 5px', '10px'),
            'value' => get_type_text('iks_image_border_radius')
        ),
        'iks_image_stretching' => array(
            'name' => __('Image stretching'),
            'description' => 'CSS background-size',
            'value' => get_type_select('iks_image_stretching', array('cover' => __('Cover'), 'contain' => __('Contain'), '100% auto' => '100% auto', '100% 100%' => '100% 100%', 'auto 100%' => 'auto 100%', 'auto auto' => 'auto auto'))
        ),
        'iks_image_quality' => array(
            'name' => __('Image size'),
            'description' => __('Choose image quality (Wordpress size)'),
            'value' => get_type_select('iks_image_quality', array('full' => __('Full'), 'large' => __('Large'), 'medium' => __('Medium (Optimal)'), 'thumbnail' => __('Thumbnail')))
        ),
        'iks_no_image_action' => array(
            'name' => __('No Image Action'),
            'description' => __('What we should do, if no image found in post'),
            'value' => get_type_select('iks_no_image_action', array('not_show' => __('Show No image'), 'standard' => __('Show Standard image')))
        ),
    );
    print_settings_block(__('Appearance of images'), '', $settings);

    echo '<div class="settings-row">';
    /* Link Button */
    $settings = array(
        'iks_button_width' => array(
            'name' => __('Width'),
            'description' => sprintf(__('from %d to %d percents'), 1, 100),
            'value' => get_type_number('iks_button_width', '1', '100')
        ),
        'iks_button_align' => array(
            'name' => __('Align'),
            'description' => __('left, center, right'),
            'value' => get_type_select_horizontal_align('iks_button_align'),
        ),
        'iks_button_text_align' => array(
            'name' => __('Text-align'),
            'description' => __('left, center, right'),
            'value' => get_type_select_horizontal_align('iks_button_text_align'),
        ),
        'iks_button_margin' => array(
            'name' => __('Margin'),
            'description' => __('top right bottom left'),
            'examples' => array('3px 3px 5px 5px', '10px'),
            'value' => get_type_text('iks_button_margin')
        ),
        'iks_button_padding' => array(
            'name' => __('Padding'),
            'description' => __('top right bottom left'),
            'examples' => array('3px 3px 5px 5px', '10px'),
            'value' => get_type_text('iks_button_padding')
        ),
        'iks_button_box_shadow' => array(
            'name' => __('Box-Shadow'),
            'description' => __('x y blur spread color |inset|initial|inherit'),
            'examples' => array('0 0 5px #000', '2px 2px 0 0 #aaabbb'),
            'value' => get_type_text('iks_button_box_shadow')
        ),
        'iks_button_border' => array(
            'name' => __('Border'),
            'description' => __('width || style || color'),
            'examples' => array('1px solid #000', '10px dashed #eaeaea'),
            'value' => get_type_text('iks_button_border')
        ),
        'iks_button_border_radius' => array(
            'name' => __('Border-radius'),
            'description' => __('top-left top-right bottom-right bottom-left'),
            'examples' => array('3px 3px 5px 5px', '10px'),
            'value' => get_type_text('iks_button_border_radius')
        ),
        'iks_button_background_color' => array(
            'name' => __('Background color'),
            'description' => '',
            'value' => get_type_color('iks_button_background_color')
        ),
        'iks_button_text_color' => array(
            'name' => __('Text color'),
            'description' => '',
            'value' => get_type_color('iks_button_text_color')
        ),
    );
    print_settings_block(__('Link button'), '', $settings);

    /* Hover Link Button */
    $settings = array(
        'iks_button_box_shadow_hover' => array(
            'name' => __('Box-Shadow'),
            'description' => __('x y blur spread color |inset|initial|inherit'),
            'examples' => array('0 0 5px #000', '2px 2px 0 0 #aaabbb'),
            'value' => get_type_text('iks_button_box_shadow_hover')
        ),
        'iks_button_border_hover' => array(
            'name' => __('Border'),
            'description' => __('width || style || color'),
            'examples' => array('1px solid #000', '10px dashed #eaeaea'),
            'value' => get_type_text('iks_button_border_hover')
        ),
        'iks_button_border_radius_hover' => array(
            'name' => __('Border-radius'),
            'description' => __('top-left top-right bottom-right bottom-left'),
            'examples' => array('3px 3px 5px 5px', '10px'),
            'value' => get_type_text('iks_button_border_radius_hover')
        ),
        'iks_button_background_color_hover' => array(
            'name' => __('Background color'),
            'description' => '',
            'value' => get_type_color('iks_button_background_color_hover')
        ),
        'iks_button_text_color_hover' => array(
            'name' => __('Text color'),
            'description' => '',
            'value' => get_type_color('iks_button_text_color_hover')
        ),
    );
    print_settings_block(__('Link button on Mouse hover'), '', $settings);
    echo '</div>';

    /* Each shortcode content */
    $settings = array(
        'iks_is_enabled_content_settings' => array(
            'name' => __('Enable this settings?'),
            'description' => __('Enable or Disable this settings for all shortcodes'),
            'value' => get_type_checkbox('iks_is_enabled_content_settings')
        ),
        'iks_is_enabled_image' => array(
            'name' => __('Image'),
            'description' => '',
            'value' => get_type_checkbox('iks_is_enabled_image')
        ),
        'iks_is_enabled_title' => array(
            'name' => __('Title'),
            'description' => '',
            'value' => get_type_checkbox('iks_is_enabled_title')
        ),
        'iks_is_enabled_comments_link' => array(
            'name' => __('Comments Link'),
            'description' => '',
            'value' => get_type_checkbox('iks_is_enabled_comments_link')
        ),
        'iks_is_enabled_date' => array(
            'name' => __('Date'),
            'description' => '',
            'value' => get_type_checkbox('iks_is_enabled_date')
        ),
        'iks_is_enabled_categories' => array(
            'name' => __('Categories'),
            'description' => '',
            'value' => get_type_checkbox('iks_is_enabled_categories')
        ),
        'iks_is_enabled_content_preview' => array(
            'name' => __('Content Preview'),
            'description' => '',
            'value' => get_type_checkbox('iks_is_enabled_content_preview')
        ),
        'iks_is_enabled_button_link' => array(
            'name' => __('Button Link'),
            'description' => '',
            'value' => get_type_checkbox('iks_is_enabled_button_link')
        ),
        'iks_button_link_text' => array(
            'name' => __('Button Text'),
            'description' => '',
            'value' => get_type_text('iks_button_link_text')
        ),
        'iks_is_enabled_pagination' => array(
            'name' => __('Pagination'),
            'description' => '',
            'value' => get_type_checkbox('iks_is_enabled_pagination')
        ),
    );
    print_settings_block(__('Content of each post'), 'IF ENABLED - This settings works for ALL shortcodes, regardless of values in shortcodes', $settings);

    /* Appearance of each post */
    $settings = array(
        'iks_post_custom_css' => array(
            'name' => __('Custom CSS for each post'),
            'value' => get_type_textarea('iks_post_custom_css')
        ),
        'iks_custom_css' => array(
            'name' => __('Custom CSS'),
            'value' => get_type_textarea('iks_custom_css')
        ),
    );
    print_settings_block(__('Other'), '', $settings, $is_full_width = true);

    print_save_button();

    echo '</form>';

    print_reset_button();

    echo '</div>';
}