<?php

/**
 * @param $class
 * @param $property
 * @param $option
 * @param string $property_end
 */
function iks_print_css( $class, $property, $option, $property_end='' ) {
	$option_value = get_theme_mod( $option );
	if ( ! empty( $option_value ) ) {
		echo $class . '{' . $property . $option_value . $property_end . ';}';
	}
}

/* 
$css = 	array (
		'class' => class,
		'settings' => array(
							array( property, option, property_end ),
							array( property, option, property_end ),
							...,
							array( property, option, property_end )
							)
		)
*/
function iks_print_grouped_css( $css ) {	
	echo $css['class'] . '{';
		foreach( $css['settings'] as $setting ) {
			$option_value = get_option( $setting[1] );
			if ( ! empty( $option_value ) ) {
				echo $setting[0] . $option_value . $setting[2] . ';';
			}
		}		
	echo '}';
}

/**
 * @param $columns_width
 * @param $margin_right
 * @param $margin_bottom
 * @param $columns
 * @return string
 */
function get_column_widths_css($columns_width, $margin_right, $margin_bottom, $columns) {
	return
	'.iks-wrap {
		width: ' . $columns_width . '%;
		margin-right: ' . $margin_right . '%;
		margin-bottom: ' . $margin_bottom . 'px;' .
	'}' .
	'.iks-wrap:nth-child(' . $columns . 'n+' . $columns . ') {
		margin-right: 0;
	}';
}

/**
 *
 */
add_action( 'wp_head', 'iks_customize_css');
function iks_customize_css() {
	?>
		<!-- dynamic styles from plugin options -->
		 <style id="iks-post-grid-css-options" type="text/css">
			<?php 
			/* custom css */
			echo get_option( 'iks_custom_css_styles' );

			/* COLUMNS */
			$is_adaptive_enabled = get_option( 'iks_is_adaptive_enabled' );
			$computer_screen_width = get_option( 'iks_computer_screen_width' );
			$mobile_screen_width = get_option( 'iks_mobile_screen_width' );
			$computer_screen_width = ( !empty($computer_screen_width) && $computer_screen_width >= 1100 ) ? $computer_screen_width : 1100; // 1100 - default computer screen width
			$mobile_screen_width = ( !empty($mobile_screen_width) && $mobile_screen_width <= 1099 ) ? $mobile_screen_width : 768; // 768 - default computer screen width
			
			/* Computer COLUMNS */
			$columns = get_option( 'iks_computer_columns_count' );
			$margin_right = get_option( 'iks_computer_between_columns_width' );
			$margin_bottom = get_option( 'iks_computer_between_rows_width' );

			$columns = ( !empty($columns) && $columns <= 10 ) ? $columns : 3; // 10 - max columns count
			$margin_right = ( !empty($margin_right) && $margin_right <= 100 ) ? $margin_right : 1; // 100 - max margin right
			$columns_width = (int) ((100 - $margin_right * ($columns-1)) / $columns);  // 100 - max page width
			/* Print */

			if ($is_adaptive_enabled) {
			    echo '@media screen and (min-width:' . $computer_screen_width . 'px) {';
			        echo get_column_widths_css($columns_width, $margin_right, $margin_bottom, $columns);
			    echo '}';

			    /* Tablet COLUMNS */
                $columns = get_option( 'iks_tablet_columns_count' );
                $margin_right = get_option( 'iks_tablet_between_columns_width' );
                $margin_bottom = get_option( 'iks_tablet_between_rows_width' );

                $columns = ( !empty($columns) && $columns <= 10 ) ? $columns : 3; // 10 - max columns count
                $margin_right = ( !empty($margin_right) && $margin_right <= 100 ) ? $margin_right : 1; // 100 - max margin right
                $columns_width = (int) ((100 - $margin_right * ($columns-1)) / $columns);  // 100 - max page width
                /* Print */
                echo '@media screen and (min-width:' . $mobile_screen_width . 'px) and (max-width: ' . ($computer_screen_width-1) . 'px) {';
                    echo get_column_widths_css($columns_width, $margin_right, $margin_bottom, $columns);
                echo '}';

                /* Mobile COLUMNS */
                $columns = get_option( 'iks_mobile_columns_count' );
                $margin_right = get_option( 'iks_mobile_between_columns_width' );
                $margin_bottom = get_option( 'iks_mobile_between_rows_width' );

                $columns = ( !empty($columns) && $columns <= 10 ) ? $columns : 3; // 10 - max columns count
                $margin_right = ( !empty($margin_right) && $margin_right <= 100 ) ? $margin_right : 1; // 100 - max margin right
                $columns_width = (int) ((100 - $margin_right * ($columns-1)) / $columns);  // 100 - max page width
                /* Print */
                echo '@media screen and (max-width: ' . ($mobile_screen_width-1) . 'px) {';
                    echo get_column_widths_css($columns_width, $margin_right, $margin_bottom, $columns);
                echo '}';

			} else {
			    echo get_column_widths_css($columns_width, $margin_right, $margin_bottom, $columns);
			}

			$css = 	array (
                'class' => '.iks-image',
                'settings' => array(
                                    array( 'height:', 'iks_image_height', 'px !important' ),
                                    array( 'margin:', 'iks_image_margin', '' ),
                                    array( 'padding:', 'iks_image_padding', '' ),
                                    array( 'box-shadow:', 'iks_image_box_shadow', '' ),
                                    array( '-webkit-box-shadow:', 'iks_image_box_shadow', '' ),
                                    array( '-moz-box-shadow:', 'iks_image_box_shadow', '' ),
                                    array( 'border:', 'iks_image_border', '' ),
                                    array( 'border-radius:', 'iks_image_border_radius', '' ),
                                    array( 'background-size:', 'iks_image_stretching', ' !important' ),
                                    )
                );
			iks_print_grouped_css($css);

			$css = 	array (
                'class' => '.iks-wrap',
                'settings' => array(
                                    array( '', 'iks_post_custom_css', '' ),
                                    array( 'box-shadow:', 'iks_post_box_shadow', '' ),
                                    array( '-webkit-box-shadow:', 'iks_post_box_shadow', '' ),
                                    array( '-moz-box-shadow:', 'iks_post_box_shadow', '' ),
                                    array( 'border:', 'iks_post_border', '' ),
                                    array( 'border-radius:', 'iks_post_border_radius', '' ),
                                    )
                );
			iks_print_grouped_css($css);

			$css = 	array (
                'class' => '.iks-inside-wrap',
                'settings' => array(
                                    array( 'padding:', 'iks_post_padding', '' ),
                                    array( 'text-align:', 'iks_post_text_align', '' ),
                                    )
                );
			iks_print_grouped_css($css);

			$css = 	array (
                'class' => '.iks-button-wrap',
                'settings' => array(
                                    array( 'text-align:', 'iks_button_align', '' ),
                                    )
                );
			iks_print_grouped_css($css);

			$css = 	array (
                'class' => '.iks-button',
                'settings' => array(
                                    array( 'width:', 'iks_button_width', '%' ),
                                    array( 'text-align:', 'iks_button_text_align', '' ),
                                    array( 'margin:', 'iks_button_margin', '' ),
                                    array( 'padding:', 'iks_button_padding', '' ),
                                    array( 'box-shadow:', 'iks_button_box_shadow', '' ),
                                    array( '-webkit-box-shadow:', 'iks_button_box_shadow', '' ),
                                    array( '-moz-box-shadow:', 'iks_button_box_shadow', '' ),
                                    array( 'border:', 'iks_button_border', '' ),
                                    array( 'border-radius:', 'iks_button_border_radius', '' ),
                                    array( 'background-color:', 'iks_button_background_color', '' ),
                                    array( 'color:', 'iks_button_text_color', '!important' ),
                                    )
                );
			iks_print_grouped_css($css);

			$css = 	array (
                'class' => '.iks-button:hover',
                'settings' => array(
                                    array( 'box-shadow:', 'iks_button_box_shadow_hover', '' ),
                                    array( '-webkit-box-shadow:', 'iks_button_box_shadow_hover', '' ),
                                    array( '-moz-box-shadow:', 'iks_button_box_shadow_hover', '' ),
                                    array( 'border:', 'iks_button_border_hover', '' ),
                                    array( 'border-radius:', 'iks_button_border_radius_hover', '' ),
                                    array( 'background-color:', 'iks_button_background_color_hover', '' ),
                                    array( 'color:', 'iks_button_text_color_hover', ' !important' ),
                                    )
                );
			iks_print_grouped_css($css);

			?>
		 </style>
		 
	<?php
}