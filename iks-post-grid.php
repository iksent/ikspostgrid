<?php
/*
Plugin Name: Iks Post Grid
Plugin URI: http://example.com
Description: Позволяет выводить любые записи Wordpress. Облегчает жизнь!
Version: 0.0.2
Author: Iksent
Author URI: http://vk.com/iksent
*/

/**
 * create custom plugin settings menu
 */
$PLUGIN_DIR_PATH = plugin_dir_path(__FILE__);
$PLUGIN_DIR_URL = plugin_dir_url(__FILE__);
include($PLUGIN_DIR_PATH . 'options.php');
include($PLUGIN_DIR_PATH . 'css-options.php');
// TODO:
//include_once dirname( __FILE__ ) . '/options.php';

// default plugins settings register
register_activation_hook(__FILE__, 'iks_init_default_settings');

/**
 * Include CSS file for plugin.
 */
add_action('admin_menu', 'iks_include_admin_styles');
function iks_include_admin_styles() {
    wp_enqueue_style('iks-admin-styles', plugin_dir_url(__FILE__) . 'admin.css', false);
}

/**
 * Include CSS file for plugin.
 */
add_action('wp_enqueue_scripts', 'iks_include_main_styles');
function iks_include_main_styles() {
    wp_enqueue_style('iks-main-styles', plugin_dir_url(__FILE__) . 'ipg-main-styles.css', false);
}


/**
 * Tag Sort
 * @param $atts array   Attributes of Shortcode
 * @param $post WP_Post Wordpress Grid Post
 * @return string
 */
function iks_get_tag_sort($atts, $post) {
    $output = "";

//    /* Tags sort */
//    if ($atts['tagsort'] == '1') {
//        $posts_tag = ($_GET[tag]) ? $_GET[tag] : "";
//        $output .= '<div class="ipg_marksort_wrap">';
//        $output .= '<a class="item" href="' . get_the_permalink() . '?tag=Магнитогорск">Магнитогорск</a>';
//        $output .= '<a class="item" href="' . get_the_permalink() . '?tag=Нижневартовск">Нижневартовск</a>';
//        $output .= '<a class="item" href="' . get_the_permalink() . '">Показать все</a>';
//        if ($posts_tag != "") {
//            $output .= '<div class="current_tag">Текущий город: ' . $posts_tag . '</div>';
//        } else {
//            $output .= '<div class="current_tag">Текущий город не выбран.</div>';
//        }
//        $output .= '</div>';
//    }
    return $output;
}

/**
 * Tag Sort
 * @param $atts array   Attributes of Shortcode
 * @param $post WP_Post Wordpress Grid Post
 * @return string
 */
function iks_get_custom_meta_key_sort($atts, $post, $posts_orderby, $posts_order, $posts_meta_key) {
    $output = "";

    $custom_meta_key_1 = $atts['cmk_1'];
    $custom_meta_key_1_label = $atts['cmk_1_label'];

    $custom_meta_key_2 = $atts['cmk_2'];
    $custom_meta_key_2_label = $atts['cmk_2_label'];

    /* Print sorting filters */

    $output .= '<div class="sort_block">';

    /* Filter one */
    $output .= '<div class="sort_select_block">';
    $output .= '<div class="sort_label"> Сортировать по: </div>';
    $output .= '<select class="myselect" name="event-dropdown1" onchange="document.location.href=this.options[this.selectedIndex].value;">';

    $tmp_opt = ($posts_orderby == "date") ? "" : get_the_permalink() . '?orderby=date&order=' . $posts_order . '&curpage=1#iks_top_anchor';

    $output .= '<option ';
    if ($tmp_opt == "") $output .= 'selected';
    $output .= ' value="' . $tmp_opt . '">Дате</option>';

    if ($custom_meta_key_1) {
        $tmp_opt = ($posts_meta_key == $custom_meta_key_1) ? "" : get_the_permalink() . '?orderby=meta_value_num&meta_key=' . $custom_meta_key_1 . '&order=' . $posts_order . '&curpage=1#iks_top_anchor';
        $output .= '<option ';
        if ($tmp_opt == "") $output .= 'selected';
        $output .= ' value="' . $tmp_opt . '">' . $custom_meta_key_1_label . '</option>';
    }

    if ($custom_meta_key_2) {
        $tmp_opt = ($posts_meta_key == $custom_meta_key_2) ? "" : get_the_permalink() . '?orderby=meta_value_num&meta_key=' . $custom_meta_key_2 . '&order=' . $posts_order . '&curpage=1#iks_top_anchor';
        $output .= '<option ';
        if ($tmp_opt == "") $output .= 'selected';
        $output .= ' value="' . $tmp_opt . '">' . $custom_meta_key_2_label . '</option>';
    }

    $output .= '</select>';
    $output .= '</div>';

    /* Print sorting filters */
    /* Filter two (by Time) */
    $output .= '<div class="sort_select_block">';
    $output .= '<div class="sort_label"> Показать по: </div>';
    $output .= '<select class="myselect" name="event-dropdown2" onchange="document.location.href=this.options[this.selectedIndex].value;">';

    $tmp_opt = ($posts_order == "DESC") ? "" : get_the_permalink() . '?order=DESC&orderby=' . $posts_orderby . '&meta_key=' . $posts_meta_key . '&curpage=1#iks_top_anchor';

    $output .= '<option ';
    if ($tmp_opt == "") $output .= 'selected';
    $output .= ' value="' . $tmp_opt . '">Убыванию</option>';

    $tmp_opt = ($posts_order == "ASC") ? "" : get_the_permalink() . '?order=ASC&orderby=' . $posts_orderby . '&meta_key=' . $posts_meta_key . '&curpage=1#iks_top_anchor';

    $output .= '<option ';
    if ($tmp_opt == "") $output .= 'selected';
    $output .= ' value="' . $tmp_opt . '">Возрастанию</option>';

    $output .= '</select>';
    $output .= '</div>';

    $output .= '<div style="clear:both"></div>';

    $output .= '</div>';
    return $output;
}

/**
 * Image
 * @param $atts array   Attributes of Shortcode
 * @param $post WP_Post Wordpress Grid Post
 * @return string
 */
function iks_get_image($atts, $post) {
    $output = "";
    global $PLUGIN_DIR_URL;

    $post_image = '';
    $no_image_action = get_option('iks_no_image_action');
    $css_image = isset($atts['css_image']) ? ($atts['css_image']) : 'iks-image';

    // get image
    $post_thumbnail_id = get_post_thumbnail_id($post->ID);
    if ($post_thumbnail_id != '') {
        // if image exists
        $post_image = wp_get_attachment_image_src($post_thumbnail_id, get_option('iks_image_quality'))[0];
    } else {
        // if image not exists
        if ($no_image_action == 'standard') { // show standard image
            $post_image = $PLUGIN_DIR_URL . 'images/image-not-found.png';
            $css_image .= ' image-not-found';
        } else if ($no_image_action == 'not_show') { // not show image
            $post_image = '';
        }
    }

    // print image
    if ($post_image != '') {
        if ($atts['image_link'] != '0') {
            $output .= '<a href="' . $post->guid . '" class="' . $css_image . '" style="background: url(' . $post_image . ');"></a>';
        } else {
            $output .= '<div class="' . $css_image . '" style="background: url(' . $post_image . ');"></div>';
        }
    }
    return $output;
}

/**
 * Title
 * @param $atts array   Attributes of Shortcode
 * @param $post WP_Post Wordpress Grid Post
 * @return string
 */
function iks_get_title($atts, $post) {
    $output = "";

    $css_title = isset($atts['css_title']) ? ($atts['css_title']) : 'iks-title';
    $title_words = isset($atts['title_words']) ? ($atts['title_words']) : 7;
    $title_end = isset($atts['title_end']) ? ($atts['title_end']) : '...';
    if ($atts['title_link'] != '0') {
        $output .= '<a href="' . $post->guid . '" class="' . $css_title . '">' . wp_trim_words($post->post_title, $title_words, $title_end) . '</a>';
    } else {
        $output .= '<text class="' . $css_title . '">' . wp_trim_words($post->post_title, $title_words, $title_end) . '</text>';
    }
    return $output;
}

/**
 * Comments
 * @param $atts array   Attributes of Shortcode
 * @param $post WP_Post Wordpress Grid Post
 * @return string
 *    // TODO: подгрузить иконки в папку
 */
function iks_get_comments_link($atts, $post) {
    $output = "";

    $css_comments = ($atts['css_comments']) ? ($atts['css_comments']) : 'iks-comments';
    $icon = ($atts['comments_icon']) ? ($atts['comments_icon']) : 'fa-comments';
    $output .= '<div class="' . $css_comments . '"><i class="fa ' . $icon . '"></i> <a href="' . get_comments_link() . '" class="iks-post-comments">' . $post->comment_count . '</a></div>';
    return $output;
}

/**
 * Date
 * @param $atts array   Attributes of Shortcode
 * @param $post WP_Post Wordpress Grid Post
 * @return string
 * F j, Y - Январь 17, 2017
 *
 * TODO: подгрузить иконки в папку
 */
function iks_get_date($atts, $post) {
    $output = "";

    $css_date = ($atts['css_date']) ? ($atts['css_date']) : 'iks-date';
    $icon = ($atts['date_icon']) ? ($atts['date_icon']) : 'icon-clock';     // TODO: подгрузить иконки в папку
    $output .= '<div class="' . $css_date . '"><i class="' . $icon . '"></i>' . mysql2date('F j, Y', $post->post_date) . '</div>';
    return $output;
}

/**
 * Categories
 * @param $atts array   Attributes of Shortcode
 * @param $post WP_Post Wordpress Grid Post
 * @return string
 *
 */
function iks_get_categories($atts, $post) {
    $output = "";

    $categories = get_the_category($post->ID);
    if ($categories) {
        $css_categories = ($atts['css_categories']) ? ($atts['css_categories']) : 'iks-categories-wrap';
        $css_categories_label = ($atts['css_categories_label']) ? ($atts['css_categories_label']) : 'iks-categories-label';
        $css_categories_list = ($atts['css_categories_list']) ? ($atts['css_categories_list']) : 'iks-categories-list';
        $css_category_item = ($atts['css_category_item']) ? ($atts['css_category_item']) : 'iks-item';

        $output .= '<div class="' . $css_categories . '">';
        $category_label = $atts['category_label'];
        if ($category_label) {
            $output .= '<text class="' . $css_categories_label . '">' . $category_label . '</text>';
        }
        $output .= '<ul class="' . $css_categories_list . '">';
        foreach ($categories as $category) {
            if ($category->cat_ID != '3') {
                $output .= '<li class="' . $css_category_item . '"><i class="fa fa-tags"></i><text> ' . $category->cat_name . '</text></li>';
            }
        }
        $output .= '</ul>';
        $output .= '</div>';
    }
    return $output;
}

/**
 * Categories
 * @param $atts array   Attributes of Shortcode
 * @param $post WP_Post Wordpress Grid Post
 * @return string
 */
function iks_get_content_preview($atts, $post) {
    $output = "";

    $css_content = isset($atts['css_content']) ? ($atts['css_content']) : 'iks-content-preview';
    if ($post->post_content != "") {
        $content_words = isset($atts['content_words']) ? ($atts['content_words']) : 15;
        $content_end = isset($atts['content_end']) ? ($atts['content_end']) : '...';
        $output .= '<div class="' . $css_content . '">';
        $output .= '<text class="text"> ' . wp_trim_words($post->post_content, $content_words, $content_end);
        if ($atts['display_read_more'] != '0') {
            if (strlen($post->post_content) > $content_words) {
                $read_more_text = isset($atts['read_more_text']) ? ($atts['read_more_text']) : "Читать полностью";
                $output .= '<a class="iks-read-more" href="' . get_the_permalink() . '">' . $read_more_text . '</a>';
            }
        }
        $output .= '</text>';
        $output .= '</div>';
    }
    return $output;
}

/**
 * Custom Fields - Works only with plugin "ACF custom fields"
 * @param $atts array   Attributes of Shortcode
 * @param $post WP_Post Wordpress Grid Post
 * @return string
 */
function iks_get_custom_fields($atts, $post) {
    $output = "";

    // if custom fields are enable
    if ($atts['cf'] == '1') {
        $css_cf_wrap = isset($atts['css_cf_wrap']) ? ($atts['css_cf_wrap']) : 'iks-custom-fields-wrap';
        $output .= '<div class="' . $css_cf_wrap . '">';
        for ($i = 1; $i <= 10; $i++) {
            $current_cf = 'cf_' . $i;
            if (isset($atts[$current_cf])) {

                $custom_field_tmp = get_field_object($atts[$current_cf], $post->ID);
                if ($custom_field_tmp['value'] != '') {

                    $css_cf_item = isset($atts['css_cf_item']) ? ($atts['css_cf_item']) : 'iks-custom-field-item';
                    $css_cf_label = isset($atts['css_cf_label']) ? ($atts['css_cf_label']) : 'iks-label';
                    $css_cf_value = isset($atts['css_cf_value']) ? ($atts['css_cf_value']) : 'iks-value';

                    $output .= '<div class="' . $css_cf_item . '">';
                    $output .= '<text class="' . $css_cf_label . '">' . $custom_field_tmp['label'] . ': ' . '</text><text class="' . $css_cf_value . '">' . $custom_field_tmp['value'] . ' ' . $atts[$current_cf . '_postfix'] . '</text>';
                    $output .= '</div>';
                }

            }
        }
        $output .= '</div>';
    }
    return $output;
}

/**
 * Link Button
 * @param $atts array               Attributes of Shortcode
 * @param $post WP_Post             Wordpress Grid Post
 * @param $button_link_text String  Button Text
 * @return string
 */
function iks_get_link_button($atts, $post, $button_link_text) {
    $output = "";

    $css_button_wrap = isset($atts['css_button_wrap']) ? ($atts['css_button_wrap']) : 'iks-button-wrap';
    $css_button = isset($atts['css_button']) ? ($atts['css_button']) : 'iks-button';
    $output .= '<div class="' . $css_button_wrap . '"><div>
                    <a href="' . $post->guid . '" class="' . $css_button . '">' . $button_link_text . '</a>
                </div></div>';
    return $output;
}

/**
 * Pagination
 * @param $posts_per_page    int Count of Posts on each page
 * @param $posts_type        string Wordpress Post Type
 * @return string
 */
function iks_get_pagination($posts_per_page, $posts_type) {
    $output = "";

    if (($posts_per_page != -1)) {
        $post_count = wp_count_posts($posts_type)->publish;
        if ($post_count > $posts_per_page) {
            $permalink = get_the_permalink();
            $current_url = $_GET;
            $current_page = $current_url['curpage'];

            $output .= '<div class="iks-pagination-wrap">';
            $output .= '<ul class="iks-pagination">';

            // count of pages
            $pages_count = $post_count / $posts_per_page;
            if (!is_int($pages_count)) {
                $pages_count = (int)$pages_count;
                $pages_count++;
            }

            // previous page link
            $prev_page = ($current_page - 1 > 0) ? ($current_page - 1) : -1;
            if ($prev_page != -1) {
                $new_url = $current_url;
                $new_url['curpage'] = $current_page - 1;
                $new_url = http_build_query($new_url);

                $output .= '<li class="prev"><a href="' . $permalink . '?' . $new_url . '#iks_top_anchor">«</a></li>';
            }

            // all pages
            for ($i = 1; $i <= $pages_count; $i++) {
                $is_active = ($i == $current_page) ? 'class="active"' : '';
                $new_url = $current_url;
                $new_url['curpage'] = $i;
                $new_url = http_build_query($new_url);

                $output .= '<li ' . $is_active . ' ><a href="' . $permalink . '?' . $new_url . '#iks_top_anchor">' . $i . '</a></li>';
            }

            // next page link
            $next_page = ($current_page + 1 <= $pages_count) ? ($current_page + 1) : -1;
            if ($next_page != -1) {
                $new_url = $current_url;
                $new_url['curpage'] = $next_page;
                $new_url = http_build_query($new_url);

                $output .= '<li class="next"><a href="' . $permalink . '?' . $new_url . '#iks_top_anchor">»</a></li>';
            }

            $output .= '</ul>';
            $output .= '</div>';
        }
    }
    return $output;
}

/**
 * Shortcode for plugin
 */
// TODO change shortcode
add_shortcode('iks_post_grid123', 'sc_iks_post_grid');
if (!function_exists('sc_iks_post_grid')) {
    function sc_iks_post_grid($atts) {
        global $post;

        $start_time = microtime(true);

        if (get_option('iks_is_enabled_content_settings') == '1') {
            // get values from settings menu
            $is_enabled_image = (get_option('iks_is_enabled_image') == '1') ? true : false;
            $is_enabled_title = (get_option('iks_is_enabled_title') == '1') ? true : false;
            $is_enabled_comments_link = (get_option('iks_is_enabled_comments_link') == '1') ? true : false;
            $is_enabled_date = (get_option('iks_is_enabled_date') == '1') ? true : false;
            $is_enabled_categories = (get_option('iks_is_enabled_categories') == '1') ? true : false;
            $is_enabled_content_preview = (get_option('iks_is_enabled_content_preview') == '1') ? true : false;
            $is_enabled_button_link = (get_option('iks_is_enabled_button_link') == '1') ? true : false;
            if ($is_enabled_button_link) {
                $button_link_text = ((get_option('iks_button_link_text')) != '') ? get_option('iks_button_link_text') : __('Read More1');
            }
            $is_enabled_pagination = (get_option('iks_is_enabled_pagination') == '1') ? true : false;
        } else {
            // get values from shortcode
            // != 0 - enabled by default
            // != 1 - disabled by default
            $is_enabled_image = ($atts['display_image'] != '0') ? true : false;
            $is_enabled_title = ($atts['display_title'] != '0') ? true : false;
            $is_enabled_comments_link = ($atts['comments'] == '1') ? true : false;
            $is_enabled_date = ($atts['date'] == '1') ? true : false;
            $is_enabled_categories = ($atts['categories'] == '1') ? true : false;
            $is_enabled_content_preview = ($atts['content'] != '0') ? true : false;
            $is_enabled_button_link = ($atts['button'] != '0') ? true : false;
            if ($is_enabled_button_link) {
                $button_link_text = (isset($atts['button_text'])) ? $atts['button_text'] : __('Read More2');
            }
            $is_enabled_pagination = ($atts['paginate'] == '1') ? true : false;
        }

        $output = '<a name="iks_top_anchor"></a>';

        $output .= '<div class="';
        $output .= isset($atts['css_iks']) ? ($atts['css_iks']) : 'iks';
        $output .= '">';

        $current_url = $_GET;

        $posts_orderby = isset($current_url['orderby']) ? ($current_url['orderby']) : (isset($atts['orderby']) ? ($atts['orderby']) : 'date');
        $posts_order = isset($current_url['order']) ? ($current_url['order']) : (isset($atts['order']) ? ($atts['order']) : 'DESC');
        $posts_meta_key = isset($current_url['meta_key']) ? ($current_url['meta_key']) : '';
        $current_page_number = isset($current_url['curpage']) ? ($current_url['curpage']) : 1;
        $posts_category_id = isset($current_url['category']) ? ($current_url['category']) : (isset($atts['category']) ? ($atts['category']) : '');
        $posts_category_name = '';
        if (!isset($posts_category_id)) {
            $posts_category_name = isset($atts['category_name']) ? ($atts['category_name']) : '';
        }
        $posts_per_page = isset($atts['posts_per_page']) ? ($atts['posts_per_page']) : -1;
        $posts_tag = isset($atts['tag_name']) ? ($atts['tag_name']) : '';
        $posts_type = isset($atts['post_type']) ? ($atts['post_type']) : 'post';

//        TODO: SORTINGS
//        /* Is sort enable? */
//        $is_sort_enable = ($atts['sort'] == '1') ? true : false;
//        if ($is_sort_enable) {
//            /* Custom Meta Keys Sort */
//            $output .= iks_get_custom_meta_key_sort($atts, $post, $posts_orderby, $posts_order, $posts_meta_key);
//        }
//
//        /* Tag Sort */
//        $output .= iks_get_tag_sort($atts, $post);

        /* Get posts */
        $args = array(
            'posts_per_page' => $posts_per_page,
            'offset' => ($atts['posts_per_page']) ? (($current_page_number - 1) * (int)$atts['posts_per_page']) : 0,
            'tax_query' => array(
                'terms' => $posts_tag
            ),
            'tag' => $posts_tag,
            'category' => $posts_category_id,
            'category_name' => $posts_category_name,
            'post_type' => $posts_type,
            'orderby' => $posts_orderby,
            'order' => $posts_order,
            'meta_key' => $posts_meta_key,
            'meta_value' => '',
            'post_mime_type' => isset($atts['post_mime_type']) ? ($atts['post_mime_type']) : '',
            'post_parent' => isset($atts['post_parent']) ? ($atts['post_parent']) : '',
            'author' => isset($atts['author']) ? ($atts['author']) : '',
            'post_status' => isset($atts['post_status']) ? ($atts['post_status']) : 'publish',
            'suppress_filters' => isset($atts['suppress_filters']) ? ($atts['suppress_filters']) : true,
            'include' => isset($atts['include']) ? ($atts['include']) : '',
            'exclude' => isset($atts['exclude']) ? ($atts['exclude']) : '',
        );
        $posts = get_posts($args);
        foreach ($posts as $post) {
            setup_postdata($post);

            /* GET CUSTOM CSS */
            $css_wrap = isset($atts['css_wrap']) ? ($atts['css_wrap']) : 'iks-wrap';
            $css_desc = isset($atts['css_desc']) ? ($atts['css_desc']) : 'iks-desc';
            $css_info = isset($atts['css_info']) ? ($atts['css_info']) : 'iks-info';

            /* Single post wrap */
            $output .= '<div class="' . $css_wrap . '">';
            /* Single post inside wrap */
            $output .= '<div class="iks-inside-wrap">';
            /* Image */
            $output .= ($is_enabled_image) ? iks_get_image($atts, $post) : '';
            /* Desc */
            $output .= '<div class="' . $css_desc . '">';
            /* Title */
            $output .= ($is_enabled_title) ? iks_get_title($atts, $post) : '';
            /* Post Info Block */
            $output .= '<div class="' . $css_info . '">';
            /* Comments */
            $output .= ($is_enabled_comments_link) ? iks_get_comments_link($atts, $post) : '';
            /* Date */
            $output .= ($is_enabled_date) ? iks_get_date($atts, $post) : '';
            $output .= '</div>'; /* Info Block */
            /* Categories */
            $output .= ($is_enabled_categories) ? iks_get_categories($atts, $post) : '';
            /* Content Preview */
            $output .= ($is_enabled_content_preview) ? iks_get_content_preview($atts, $post) : '';
            /* Custom Fields */
            $output .= iks_get_custom_fields($atts, $post);
            /* Button Read More */
            $output .= ($is_enabled_button_link) ? iks_get_link_button($atts, $post, $button_link_text) : '';
            $output .= '</div>'; /* Desc */
            $output .= '</div>'; /* Single post inside wrap */
            $output .= '</div>'; /* Single post wrap */

        }
        wp_reset_postdata();

        /* Pagination */
        $output .= ($is_enabled_pagination) ? iks_get_pagination($posts_per_page, $posts_type) : '';
        $output .= '</div>'; /* Iks */

        $end_time = microtime(true);
        echo "Exec time: " . ($end_time - $start_time);

        return $output;
    }
}
